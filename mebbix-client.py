# -*- coding: utf-8 -*-

import time, json, ConfigParser, os

import util, windows_parsing, command, socketclient
from httpcomm import Httpcomm

#const init
program_name = "mebbix-client alpha 0.1"
program_version = 5
configfilename = 'config.ini'
default_config = {
	'testmode': 0,
	'server': 'https://mebbix.gensei.cf',
	'user': 'gihun',
	'port': 443,
	'refreshtime': 5,
}

#var init
configf = None
register = False
last_updatecheck = 0

def config_read():
	global configf, config

	change = False
	
	if configf is None:
		configf = ConfigParser.ConfigParser()
		configf.read(configfilename)

	config = dict(configf.items('client'))

	for entry in default_config.keys():
		if config.get(entry, None) is None:
			change = True
			configf.set('client', entry, str(default_config[entry]))
	
	if change == True:
		config_write()
		config_read()
		return

	config['testmode'] = int(config['testmode'])
	#config['port'] = int(config['testmode'])
	config['refreshtime'] = int(config['refreshtime'])
	
def config_write():
	f = open(configfilename, 'wb')
	configf.write(f)
	f.close()
	
def init():
	global comname, minerinfo, http
	
	if config['testmode'] == 1:
		comname = u'daebo01-u530'
		minerinfo = {
			'version' : program_version,
			'ip' : '1.2.3.4',
			'mac' : '001122334455',
			'cpu' : 'i7 e go sip da',
			'board' : 'hellg notebook',
			'graphics' : 'nae jang graphics',
		}

	else:
		#리눅스 지원 추가...할일이 있을까?

		comname = windows_parsing.computer_name()
		minerinfo = {
			'version' : program_version,
			'ip' : windows_parsing.ip_address(),
			'mac' : None,
			'cpu' : windows_parsing.cpu(),
			'board' : windows_parsing.mainboard(),
			'graphics' : windows_parsing.graphics(),
		}
		minerinfo['mac'] = windows_parsing.mac_address(minerinfo['ip'])
	
	http = Httpcomm(config['server'], config['port'])
	
	if os.name == 'nt':
		os.system('cscript autostartupshortcutcreator.vbs')

def version_check():
	global server_version
	
	server_version = http.version_check()

	if server_version is None:
		print('server version is unknown')
		server_version = 0
		return

	if int(server_version) > program_version:
		print('client update!!!')
		print('local version : %s, server version : %s' % (program_version, server_version))
		
		http.update()
		if not 'start.cmd' in os.listdir('.'):
			os.system('update.cmd')
		quit()

def miner_register():
	postdata = {
		'user': config['user'],
		'comname' : comname,
		'minerinfo' : minerinfo,
	}

	recvdata = http.poweron(json.dumps(postdata))
	if recvdata:
		try:
			data = json.loads(recvdata)

		except ValueError:
			print u'miner register data decode error'

		print u'miner register success'
		return True
		#todo command execute add
		#id = data['id']
		#command.command(data['command'])

	print("miner register failed")
	return False

def miner_update():
	global register

	postdata = {
		'user': config['user'],
		'comname' : comname,
		'miner_name' : socketclient.minername,
		'miner_state' : None,
	}
	
	if config['testmode'] == 1:
		postdata['miner_name'] = 'claymore_ethereum'
		postdata['miner_state'] = '{"id": 0, "result": ["testversion", "1", "21867;1;0", "21867", "142140;1;0", "142140", "71;60", "asia1.ethereum.miningpoolhub.com:20535;asia1.nanopool.org:15555", "0;0;0;0"], "error": null}'
	else:
		postdata['miner_state'] = socketclient.getstat()

	recvdata = http.alive(json.dumps(postdata))

	if recvdata:
		try:
			data = json.loads(recvdata)
			if data['result'] == 'noregister':
				register = False

		except ValueError:
			print u'data decode error'

	else:
		print u'communication error'
		


if __name__ == "__main__":
	print("%s version %s" % (program_name, program_version))
	print("contact : webmaster@playmp3.kr")
	
	config_read()
	init()
	version_check()
	print('local version : %s, server version : %s' % (program_version, server_version))
	print("hostname : %s" % (comname))
	
	while(True):
		if int(time.time()) - last_updatecheck > 3600:
			version_check()
			last_updatecheck = int(time.time())

		if register:
			miner_update()

		else:
			register = miner_register()
			if register:
				continue
		
		time.sleep(config['refreshtime'])
