# -*- coding: utf-8 -*-

import socket
import sys
import time

minername = None

def socket_getdata(ip, port, data):
	serverinfo = (ip, port)

	sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)
	sock.settimeout(1)

	try:
		sock.connect(serverinfo)
		sock.sendall(data)

	except socket.timeout:
		print('socket timeout')
		return ''

	except socket.error:
		print('socket error')
		return ''

	recvdata = ''
	while True:
		try:
			buf = sock.recv(16)
			recvdata = recvdata + buf
			if not buf:
				break

		except socket.timeout:
			print('socket timeout')
			break

		except socket.error:
			print('socket error')
			break

	sock.close()

	return recvdata

def claymore_getstat():
	return socket_getdata('127.0.0.1', 3333, '{"id":0,"jsonrpc":"2.0","method":"miner_getstat1"}')

def ewbf_getstat():
	return socket_getdata('127.0.0.1', 3334, '{"id":1, "method":"getstat"}\n')
	
def claymore_monero_getstat():
	return socket_getdata('127.0.0.1', 3335, '{"id":0,"jsonrpc":"2.0","method":"miner_getstat1"}')

def getstat():
	global minername

	if minername == 'claymore_ethereum':
		return claymore_getstat()

	elif minername == 'ewbf_zcash':
		return ewbf_getstat()
		
	elif minername == 'claymore_monero':
		return claymore_monero_getstat()

	else:
		print('miner auto search')

		if claymore_getstat():
			minername = 'claymore_ethereum'

		elif ewbf_getstat():
			minername = 'ewbf_zcash'

		elif claymore_monero_getstat():
			minername = 'claymore_monero'
			
		else:
			print('miner search fail -- sleep 60sec')
			return None

		print('find miner! - ' + minername)
