# -*- coding:utf-8 -*-
import urllib2
import httplib
import socket
import ssl

class Httpcomm():

	def __init__(self, server, port):
		self.server = server + ':' + port

	def json_request(self, url, data):
		headers = { 'User-Agent' : 'mebbix-client alpha', 'Content-Type': 'application/json' }

		req = urllib2.Request(url, data, headers)

		try:
			ret = urllib2.urlopen(req, timeout=60).read()

		except urllib2.HTTPError as err:
			print("http error" + str(err.code))
			ret = None

		except urllib2.URLError, e:
			print("url error : " + str(e.reason))
			ret = None

		except socket.timeout:
			print('socket timeout')
			ret = None

		except socket.error as e:
			print('socket error : ' + str(e))
			ret = None

		except ssl.SSLError:
			print('ssl error')
			ret = None

		except httplib.BadStatusLine as e:
			print('http error ' + str(e))
			ret = None

		return ret

	def download_file(self, url, filename):
		headers = { 'User-Agent' : 'mebbix-client alpha', 'Content-Type': 'application/json' }
		
		req = urllib2.Request(url, None, headers)
		f = open(filename, 'wb')

		try:
			response = urllib2.urlopen(req, timeout=60)
		
		except urllib2.HTTPError as err:
			print("http error" + str(err.code))
			return

		except urllib2.URLError, e:
			print("url error : " + str(e.reason))
			return

		except socket.timeout:
			print('socket timeout')
			return

		except socket.error as e:
			print('socket error : ' + str(e))
			return

		except ssl.SSLError:
			print('ssl error')
			return

		except httplib.BadStatusLine as e:
			print('http error ' + str(e))
			return

		f.write(response.read())
		f.close()

	def alive(self, data):
		return self.json_request(self.server + '/service/agent/alive', data)

	def poweron(self, data):
		return self.json_request(self.server + '/service/agent/poweron', data)

	def reboot(self, data):
		return self.json_request(self.server + '/service/agent/reboot', data)

	def version_check(self):
		return self.json_request(self.server + '/agent/version.txt', None)

	def update_file_list(self):
		return self.json_request(self.server + '/agent/file.txt', None)
		
	def update(self):
		filelist = self.update_file_list().strip().split('\n')
		
		for a in filelist:
			self.download_file(self.server + '/agent/' + a, a)
