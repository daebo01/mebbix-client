# -*- coding: utf-8 -*-

import re

import util

def computer_name():
	data = util.command_execute_with_data(u'wmic computersystem get name')
	data = unicode(data, encoding = 'euc-kr')
	return data.split("\n")[1].strip()

def ip_address():
	#추후 default gateway가 여러개 일시 가장 metric이 낮은걸 고르는 기능 추가하기...
	data = util.command_execute_with_data(u'route print -4 0.0.0.0')
	data = unicode(data, encoding = 'euc-kr')

	tempmatch = re.findall(u'(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\s+(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\s+(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\s+(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])\s+([0-9]+)', data)

#	import pprint
#	pprint.pprint(tempmatch)
	return u'%s.%s.%s.%s' % (tempmatch[0][12], tempmatch[0][13], tempmatch[0][14], tempmatch[0][15])

def mac_address(ip):
	data = util.command_execute_with_data(u'ipconfig /all')
	data = unicode(data, encoding = 'euc-kr')

	for a in data.split('\r\n\r\n'):
		if a.find(ip) != -1:
			tempmatch = re.findall('([0-9A-Fa-f]{2})[:-]([0-9A-Fa-f]{2})[:-]([0-9A-Fa-f]{2})[:-]([0-9A-Fa-f]{2})[:-]([0-9A-Fa-f]{2})[:-]([0-9A-Fa-f]{2})', a)
			return u'%s%s%s%s%s%s' % (tempmatch[0][0], tempmatch[0][1], tempmatch[0][2], tempmatch[0][3], tempmatch[0][4], tempmatch[0][5])

	return None

def cpu():
	data = util.command_execute_with_data(u'wmic cpu get name')
	data = unicode(data, encoding = 'euc-kr')
	return data.split("\n")[1].strip()

def mainboard():
	data = util.command_execute_with_data(u'wmic baseboard get product,Manufacturer,version,serialnumber')
	data = unicode(data, encoding = 'euc-kr')
	return data.split("\n")[1].strip()

def graphics():
	data = util.command_execute_with_data(u'wmic path win32_VideoController get name')
	data = unicode(data, encoding = 'euc-kr')
	return data.split("\n")[1].strip()
