# -*- coding: utf-8 -*-

import subprocess, os, re

def command_execute(command):
	os.system(command)
	#subprocess.call(command, parameter)
	return True

def command_execute_with_data(command):
	#추후 command auto quota insert
	return subprocess.check_output(command)

def regex_match(regex, data):
	patt = re.compile(regex, re.UNICODE)
	return patt.match(data)

